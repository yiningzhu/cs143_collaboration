class C inherits Man {
	a : Int;
	b : Bool;
	init(x : Int, y : Bool) : C {
           {
		a <- x;
		b <- y;
		self;
           }
	};
};

Class Main {
	main():C {
	  (new C).init(1,true)
	};
};
Class Man  {
	main():C {
	  (new C).init(1,true)
	};
};
Class Many inherits IO {

	main():C {
	  (new C).init(1,true)
	};
};


Class IOO {

	main():C {
	  (new C).init(1,true)
	};
};
Class Intt {

	main():C {
	  (new C).init(1,true)
	};
};
