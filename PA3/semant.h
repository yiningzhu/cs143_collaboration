#ifndef SEMANT_H_
#define SEMANT_H_

#include <assert.h>
#include <iostream>  
#include "cool-tree.h"
#include "stringtab.h"
#include "symtab.h"
#include "list.h"
#include <list>
#include <map>

#define TRUE 1
#define FALSE 0

class ClassTable;
typedef ClassTable *ClassTableP;

// This is a structure that may be used to contain the semantic
// information such as the inheritance graph.  You may use it or not as
// you like: it is only here to provide a container for the supplied
// methods.

class ClassTable {
private:
  int semant_errors;
  std::map<Symbol, Class_> class_map;
	
  SymbolTable<Symbol, Class__class> *classes_scope;
  void install_basic_classes();
  ostream& error_stream;

public:
  ClassTable(Classes);
  
  bool check_ancestor(Symbol ancestor, Symbol child);
  std::list<Symbol> inheritance_Path(Symbol a);
  Symbol Common_Ancestor(Symbol a, Symbol b);
  int errors() { return semant_errors; }
  ostream& semant_error();
  ostream& semant_error(Class_ c);
  ostream& semant_error(Symbol filename, tree_node *t);
};


#endif

