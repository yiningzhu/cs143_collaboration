/*
 *  The scanner definition for COOL.
 */

/*
 *  Stuff enclosed in %{ %} in the first section is copied verbatim to the
 *  output, so headers and global definitions are placed here to be visible
 * to the code in the file.  Don't remove anything that was here initially
 */

%{
#include <cool-parse.h>
#include <stringtab.h>
#include <utilities.h>
#include <string.h>

/* The compiler assumes these identifiers. */
#define yylval cool_yylval
#define yylex  cool_yylex

/* Max size of string constants */
#define MAX_STR_CONST 1025
#define YY_NO_UNPUT   /* keep g++ happy */

extern FILE *fin; /* we read from this file */

/* define YY_INPUT so we read from the FILE fin:
 * This change makes it possible to use this scanner in
 * the Cool compiler.
 */
#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) \
	if ( (result = fread( (char*)buf, sizeof(char), max_size, fin)) < 0) \
		YY_FATAL_ERROR( "read() in flex scanner failed");

char string_buf[MAX_STR_CONST]; /* to assemble string constants */
char *string_buf_ptr;
int char_count = 0;
int nested_comment_count = 0;

extern int curr_lineno;
extern int verbose_flag;

extern YYSTYPE cool_yylval;

/*
 *  Add Your own definitions here
 */

%}

/*
 * Define names for regular expressions here.
 */
/* 
 * Define 10.1 Integers, Identifiers, and Special Notation.
 */
DIGIT           [0-9]
INT             [0-9]+
TRUE            t?i:rue
FALSE           f?i:alse
TYPEID          [A-Z][a-zA-Z0-9_]*  
/* including self */
OBJECTID        [a-z][a-zA-Z0-9_]*  
/* including SELF_TYPE */
/*Type identifiers begin with a capital letter; object identifiers begin with a lower case letter*/
/* 
 * Define 10.4 Keywords.
 */
CLASS           ?i:class
ELSE            ?i:else
FI              ?i:fi
IF              ?i:if
IN              ?i:in
INHERITS        ?i:inherits
ISVOID          ?i:isvoid
LET             ?i:let
LOOP            ?i:loop
POOL            ?i:pool
THEN            ?i:then
WHILE           ?i:while
CASE            ?i:case
ESAC            ?i:esac
NEW             ?i:new
OF              ?i:of
NOT             ?i:not

OTHER_SN      [{};:,\.()<=\+\-~@\*/]

DARROW          =>
ASSIGN          <-
LE              <=


%x STRING
%x COMMENT
%x INLINE_COMMENT

%%

 /*
  *  Nested comments
  */


 /*
  *  The multiple-character operators.
  */
{DARROW}		{ return (DARROW); }
{ASSIGN}        { return (ASSIGN); }
{LE}            { return (LE); }
\n          {printf("#%d:newline\n",curr_lineno); curr_lineno++; } /* advance line counter */
{OTHER_SN}      { return *yytext; }



 /*
  * Keywords are case-insensitive except for the values true and false,
  * which must begin with a lower-case letter.
  */

{CLASS}       {return (CLASS);}
{ELSE}            {return (ELSE);}
{FI}              {return (FI);}
{IF}              {return (IF);}
{IN}              {return (IN);}
{INHERITS}        {return (INHERITS);}
{ISVOID}          {return (ISVOID);}
{LET}             {return (LET);}
{LOOP}            {return (LOOP);}
{POOL}            {return (POOL);}
{THEN}            {return (THEN);}
{WHILE}           {return (WHILE);}
{CASE}            {return (CASE);}
{ESAC}            {return (ESAC);}
{NEW}             {return (NEW);}
{OF}              {return (OF);}
{NOT}             {return (NOT);}

{INT} {
            cool_yylval.symbol = inttable.add_string(yytext);
            return (INT_CONST);
          }
{TRUE} {
        cool_yylval.boolean = true;
        printf("#BOOL:%s",yytext);
        return (BOOL_CONST);

}

{FALSE} {
        cool_yylval.boolean = false;
        printf("#BOOL:%s",yytext);
        return (BOOL_CONST);
        
}

{TYPEID}  {        

        printf("#TYPEID:%s",yytext); 
        cool_yylval.symbol = idtable.add_string(yytext);
        return (TYPEID);
}

{OBJECTID}  {      

        printf("#OBJECTID:%s",yytext); 
        cool_yylval.symbol = idtable.add_string(yytext);
        return (OBJECTID);
}
 
 /*
  *  String constants (C syntax)
  *  Escape sequence \c is accepted for all characters c. Except for 
  *  \n \t \b \f, the result is c.
  *
  */

<INITIAL>{
  "(*" {
    BEGIN (COMMENT);
    nested_comment_count = 0;
  }

  "*)" {
    cool_yylval.error_msg = "Comment end without corresponding comment open";
    return ERROR;
  }

  "--" {
    BEGIN(INLINE_COMMENT);
  }

  \" {
    BEGIN (STRING);
    memset(string_buf,0,sizeof(string_buf));
    string_buf_ptr = string_buf;
  }

  {INHERITS} {return INHERITS;}
  {CLASS} {return CLASS;}
  {ELSE} {return ELSE;}
  {FI} {return FI;}
  {IF} {return IF;}
  {IN} {return IN;}
  {ISVOID} {return ISVOID;}
  {LET} {return LET;}
  {LOOP} {return LOOP;}
  {POOL} {return POOL;}
  {THEN} {return THEN;}
  {WHILE} {return WHILE;}
  {CASE} {return CASE;}
  {ESAC} {return ESAC;}
  {NEW} {return NEW;}
  {OF} {return OF;}
  {NOT} {return NOT;}

  {TYPEID} {
    cool_yylval.symbol = idtable.add_string(yytext);
    return TYPEID;
  }
  {OBJID} {
    cool_yylval.symbol = idtable.add_string(yytext);
    return OBJECTID;
  }

  \n  {curr_lineno++;}
}


<INLINE_COMMENT>{
  <<EOF>> {
    BEGIN (INITIAL);
  }

  \n {
    curr_lineno++;
    BEGIN(INITIAL);
  }

  [^\n]* {}
}

<COMMENT>
{
  <<EOF>> {
    cool_yylval.error_msg = "The comment contains EOF.";
    BEGIN (INITIAL);
    return ERROR;
  }

  "(*" {
    ++nested_comment_count;
  }

  \n {
    curr_lineno++;
  }

  "*)" {
    if (nested_comment_count > 0){
      --nested_comment_count;
    } else if (nested_comment_count == 0) {
      BEGIN (INITIAL);
    }
  }

  [^\n(*]* {}
  "(" {}
  "*" {}
}

<STRING>{
  <<EOF>> {
    cool_yylval.error_msg = "The string constant contains EOF.";
    BEGIN (INITIAL);
    return ERROR;
  }

  \n {
    curr_lineno++;
    cool_yylval.error_msg = "The string constant contains newline character.";
    BEGIN (INITIAL);
    return ERROR;
  }

  \0 {
    cool_yylval.error_msg = "The string constant contains null character.";
    BEGIN (INITIAL);
    return ERROR;
  }


  \" {
    cool_yylval.symbol = stringtable.add_string(string_buf);
    memset(string_buf,0,sizeof(string_buf));
    BEGIN (INITIAL);
    return STR_CONST;
  }

  \\0 {
    if(char_count == 1024){
      cool_yylval.error_msg = "The string is too long.";
      BEGIN (INITIAL);
      return ERROR;
    }
    *string_buf_ptr++ = '0';
    ++char_count;
  }
  \\b {
    if(char_count == 1024){
      cool_yylval.error_msg = "The string is too long.";
      BEGIN (INITIAL);
      return ERROR;
    }
    *string_buf_ptr++ = '\b';
    ++char_count;
  }

  \\t {
    if(char_count == 1024){
      cool_yylval.error_msg = "The string is too long.";
      BEGIN (INITIAL);
      return ERROR;
    }
    *string_buf_ptr++ = '\t';
    ++char_count;
  }

  \\n {
    if(char_count == 1024){
      cool_yylval.error_msg = "The string is too long.";
      BEGIN (INITIAL);
      return ERROR;
    }
    *string_buf_ptr++ = '\n';
    ++char_count;
  }

  \\f {
    if(char_count == 1024){
      cool_yylval.error_msg = "The string is too long.";
      BEGIN (INITIAL);
      return ERROR;
    }
    *string_buf_ptr++ = '\f';
    ++char_count;
  }
 
 \\. {
   if(char_count == 1024){
      cool_yylval.error_msg = "The string is too long.";
      BEGIN (INITIAL);
      return ERROR;
    }
    
    *string_buf_ptr++ = yytext[1];
    ++char_count;
 }

  [^\\\n\"]+ {
    char *charptr = yytext;
    while (*charptr){
      if (char_count == 1024){
        cool_yylval.error_msg = "The string is too long.";
        BEGIN(INITIAL);
        return ERROR;
      }
      ++char_count;
      *string_buf_ptr++ = *charptr++;
    }
  }

}
%%
