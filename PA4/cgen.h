#include <assert.h>
#include <stdio.h>
#include "emit.h"
#include "cool-tree.h"
#include "symtab.h"
#include <map>
#include <vector>
#include <algorithm>
#include <deque>
#include <stack>

enum Basicness     {Basic, NotBasic};
#define TRUE 1
#define FALSE 0

class CgenClassTable;
typedef CgenClassTable *CgenClassTableP;

class CgenNode;
typedef CgenNode *CgenNodeP;

class CgenClassTable : public SymbolTable<Symbol,CgenNode> {
private:
   List<CgenNode> *nds;
   ostream& str;
   int stringclasstag;
   int intclasstag;
   int boolclasstag;


// The following methods emit code for
// constants and global declarations.

   void code_global_data();
   void code_global_text();
   void code_bools(int);
   void code_select_gc();
   void code_constants();

// The following creates an inheritance graph from
// a list of classes.  The graph is implemented as
// a tree of `CgenNode', and class names are placed
// in the base class symbol table.

   void install_basic_classes();
   void install_class(CgenNodeP nd);
   void install_classes(Classes cs);
   void build_inheritance_tree();
   void set_relations(CgenNodeP nd);
public:
	std::deque<CgenNode*> class_nodes;
	std::map<Symbol, int> class_tags;
	CgenNode* GetNode(Symbol class_name);
	void code_class_nameTab();
	void code_class_objTab();
	void code_class_dispTab();
	void code_classes_protObj();
	void code_classes_init();
	void code_classes_method();
	
   CgenClassTable(Classes, ostream& str);
   void code();
   CgenNodeP root();
};


class CgenNode : public class__class {
private: 
   CgenNodeP parentnd;                        // Parent of class
   List<CgenNode> *children;                  // Children of class
   Basicness basic_status;                    // `Basic' if class is basic
                                              // `NotBasic' otherwise

public:
   CgenNode(Class_ c,
            Basicness bstatus,
            CgenClassTableP class_table);

   void add_child(CgenNodeP child);
   List<CgenNode> *get_children() { return children; }
   void set_parentnd(CgenNodeP p);
   CgenNodeP get_parentnd() { return parentnd; }
   int basic() { return (basic_status == Basic); }
	
	
	void build_disp_tab();
	
	std::vector<method_class*> all_methods; //include parent
	std::vector<method_class*> get_all_methods() ;
	void set_all_feature();
	
	std::vector<method_class*> self_methods; //not include parent
	std::vector<method_class*> get_self_methods(){return self_methods; }
	void set_self();
	
	std::vector<attr_class*> all_attri;
	std::vector<attr_class*> get_all_attri();
	//void set_all_attri();
	
	std::vector<attr_class*> self_attri;
	std::vector<attr_class*> get_self_attri();
	//void set_self_attri();
	
	int get_attri_offset(Symbol m_attri);
	
	std::map<Symbol, int> dispatch_tab_int;
	std::map<Symbol, int> get_dispatch_tab_int();
	
	
	
	void code_class_protObj(ostream& s, int classtag);
	
	void code_class_init(ostream& s, int classtag);
	void code_class_method(ostream& s, int classtag);
	
	
};

class BoolConst 
{
 private: 
  int val;
 public:
  BoolConst(int);
  void code_def(ostream&, int boolclasstag);
  void code_ref(ostream&) const;
};



class Env {
	public:
	CgenNode* curr_node;
	std::stack<int> scope;
	std::vector<Symbol> var_tab;
    std::vector<Symbol> para_tab;
	
	
	void Environment(){
		curr_node = NULL;
		
	}
	
	void enter_scope() {
		
        scope.push(0);
		
    }

    void exit_scope() {
        for (int i = 0; i < scope.top(); ++i) {
            var_tab.pop_back();
        }
        scope.pop();
    }
	
	
	

    int AddVar(Symbol s) {
        var_tab.push_back(s);
        int i = scope.top();
		scope.pop();
		scope.push(i+1);
        return var_tab.size() - 1;
    }
	
	int get_var_offset(Symbol s) {
        for (int idx = var_tab.size() - 1; idx >= 0; --idx) {
            if (var_tab[idx] == s) {
                return var_tab.size() - 1 - idx;
            }
        }
        return -1;
    }

    int AddParam(Symbol s) {
        para_tab.push_back(s);
        return para_tab.size() - 1;
    }


    int get_param_offset(Symbol s) {
        for (int idx = 0; idx < para_tab.size(); ++idx) {
            if (para_tab[idx] == s) {
                return para_tab.size() - 1 - idx;
            }
        }
        return -1;
    }

    
	
};

