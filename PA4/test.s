# start of generated code
#Building CgenClassTable
#class tag tableMan: 0
#class tag tableMain: 1
#class tag tableC: 2
#class tag tableString: 3
#class tag tableBool: 4
#class tag tableInt: 5
#class tag tableIO: 6
#class tag tableObject: 7
coding global data
	.data
	.align	2
	.globl	class_nameTab
	.globl	Main_protObj
	.globl	Int_protObj
	.globl	String_protObj
	.globl	bool_const0
	.globl	bool_const1
	.globl	_int_tag
	.globl	_bool_tag
	.globl	_string_tag
_int_tag:
	.word	5
_bool_tag:
	.word	4
_string_tag:
	.word	3
choosing gc
	.globl	_MemMgr_INITIALIZER
_MemMgr_INITIALIZER:
	.word	_NoGC_Init
	.globl	_MemMgr_COLLECTOR
_MemMgr_COLLECTOR:
	.word	_NoGC_Collect
	.globl	_MemMgr_TEST
_MemMgr_TEST:
	.word	0
coding constants
	.word	-1
str_const13:
	.word	3
	.word	5
	.word	String_dispTab
	.word	int_const1
	.byte	0	
	.align	2
	.word	-1
str_const12:
	.word	3
	.word	5
	.word	String_dispTab
	.word	int_const2
	.ascii	"Man"
	.byte	0	
	.align	2
	.word	-1
str_const11:
	.word	3
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Main"
	.byte	0	
	.align	2
	.word	-1
str_const10:
	.word	3
	.word	5
	.word	String_dispTab
	.word	int_const0
	.ascii	"C"
	.byte	0	
	.align	2
	.word	-1
str_const9:
	.word	3
	.word	6
	.word	String_dispTab
	.word	int_const4
	.ascii	"String"
	.byte	0	
	.align	2
	.word	-1
str_const8:
	.word	3
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Bool"
	.byte	0	
	.align	2
	.word	-1
str_const7:
	.word	3
	.word	5
	.word	String_dispTab
	.word	int_const2
	.ascii	"Int"
	.byte	0	
	.align	2
	.word	-1
str_const6:
	.word	3
	.word	5
	.word	String_dispTab
	.word	int_const5
	.ascii	"IO"
	.byte	0	
	.align	2
	.word	-1
str_const5:
	.word	3
	.word	6
	.word	String_dispTab
	.word	int_const4
	.ascii	"Object"
	.byte	0	
	.align	2
	.word	-1
str_const4:
	.word	3
	.word	7
	.word	String_dispTab
	.word	int_const6
	.ascii	"_prim_slot"
	.byte	0	
	.align	2
	.word	-1
str_const3:
	.word	3
	.word	7
	.word	String_dispTab
	.word	int_const7
	.ascii	"SELF_TYPE"
	.byte	0	
	.align	2
	.word	-1
str_const2:
	.word	3
	.word	7
	.word	String_dispTab
	.word	int_const7
	.ascii	"_no_class"
	.byte	0	
	.align	2
	.word	-1
str_const1:
	.word	3
	.word	8
	.word	String_dispTab
	.word	int_const8
	.ascii	"<basic class>"
	.byte	0	
	.align	2
	.word	-1
str_const0:
	.word	3
	.word	6
	.word	String_dispTab
	.word	int_const9
	.ascii	"test.cl"
	.byte	0	
	.align	2
	.word	-1
int_const9:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	7
	.word	-1
int_const8:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	13
	.word	-1
int_const7:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	9
	.word	-1
int_const6:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	10
	.word	-1
int_const5:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	2
	.word	-1
int_const4:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	6
	.word	-1
int_const3:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	4
	.word	-1
int_const2:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	3
	.word	-1
int_const1:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
int_const0:
	.word	5
	.word	4
	.word	Int_dispTab
	.word	1
	.word	-1
bool_const0:
	.word	4
	.word	4
	.word	
	.word	0
	.word	-1
bool_const1:
	.word	4
	.word	4
	.word	
	.word	1
class_nameTab:
	.word	str_const12
	.word	str_const11
	.word	str_const10
	.word	str_const9
	.word	str_const8
	.word	str_const7
	.word	str_const6
	.word	str_const5
class_objTab:
	.word	Man_protObj
	.word	Man_init
	.word	Main_protObj
	.word	Main_init
	.word	C_protObj
	.word	C_init
	.word	String_protObj
	.word	String_init
	.word	Bool_protObj
	.word	Bool_init
	.word	Int_protObj
	.word	Int_init
	.word	IO_protObj
	.word	IO_init
	.word	Object_protObj
	.word	Object_init
#code class dispatch table
Man_dispTab:
	.word	Man.hello
Main_dispTab:
	.word	Main.main
C_dispTab:
	.word	C.init
String_dispTab:
	.word	String.length
Bool_dispTab:
	.word	Bool.abort
Int_dispTab:
	.word	Int.abort
IO_dispTab:
	.word	IO.out_string
Object_dispTab:
	.word	Object.abort
	.word	-1
Man_protObj:
	.word	0	# class tag
	.word	-1
Main_protObj:
	.word	1	# class tag
	.word	-1
C_protObj:
	.word	2	# class tag
	.word	int_const1	# int(0)
	.word	-1
String_protObj:
	.word	3	# class tag
	.word	int_const1	# int(0)
	.word	-1
Bool_protObj:
	.word	4	# class tag
	.word	0	# val(0)
	.word	-1
Int_protObj:
	.word	5	# class tag
	.word	0	# val(0)
	.word	-1
IO_protObj:
	.word	6	# class tag
	.word	-1
Object_protObj:
	.word	7	# class tag
#coding global text
	.globl	heap_start
heap_start:
	.word	0
	.text
	.globl	Main_init
	.globl	Int_init
	.globl	String_init
	.globl	Bool_init
	.globl	Main.main
#code class initial methods
Man_init:
Main_init:
C_init:
String_init:
Bool_init:
Int_init:
IO_init:
Object_init:
#code class methods
Man.hello:
Main.main:
C.init:
String.length:
String.concat:
String.substr:
IO.out_string:
IO.out_int:
IO.in_string:
IO.in_int:
Object.abort:
Object.type_name:
Object.copy:

# end of generated code
